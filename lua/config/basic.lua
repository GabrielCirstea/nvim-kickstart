vim.opt.number            = true
vim.opt.relativenumber    = true
vim.opt.tabstop           = 4
vim.opt.softtabstop       = 4
vim.opt.shiftwidth        = 4
vim.opt.hlsearch          = false
vim.opt.incsearch         = true
vim.opt.wrap              = false

vim.opt.undofile          = true
vim.opt.undodir           = os.getenv( "HOME" ) .. '/.local/state/nvim/undodir'
vim.opt.path              = ".,**"
