vim.keymap.set('i', 'jk', "<esc>", { noremap=true} )
vim.keymap.set('n', '-', "ddp", { noremap=true} )
vim.keymap.set('n', '_', "ddkP", { noremap=true} )
vim.keymap.set('n', '<c-u>', "viwUe", { noremap=true} )
vim.keymap.set('i', '<c-u>', "<esc>viwUea", { noremap=true} )

vim.g.mapleader='\\'
vim.keymap.set('n', '<Leader>ev', ":vsplit $MYVIMRC<CR>", {noremap=true})
vim.keymap.set('n', '<Leader>sv', ":source $MYVIMRC<CR>", {noremap=true})

-- splits
vim.keymap.set('n', '<Leader>vl', ":leftabove vsplit #<CR>", {noremap=true})
vim.keymap.set('n', '<Leader>vr', ":rightbelow vsplit #<CR>", {noremap=true})
vim.keymap.set('n', '<Leader>sa', ":leftabove split #<CR>", {noremap=true})
vim.keymap.set('n', '<Leader>sb', ":rightbelow aplit #<CR>", {noremap=true})

-- trailing witespaces
vim.keymap.set('n', '<Leader>w', ":match Error / \\+$/<CR>", {noremap=true})
vim.keymap.set('n', '<Leader>W', ":match none<CR>", {noremap=true})

-- fuzzy finder
vim.keymap.set('n', '<Leader>F', ":FZF<CR>", {noremap=true})
